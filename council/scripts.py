from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext as _

from council.models import Category, Profile, Tax, Project, PaymentOrder

from ikwen.core.utils import add_database, get_service_instance

from pinsview.models import Pin, City


def create_permissions(db=None):
    service = get_service_instance()
    if not db:
        db = service.database
    add_database(db)
    # dashboard_ct = ContentType.objects.get_for_model(DashboardBase)
    # Permission.objects.using(db).create(codename='ik_view_dashboard', name=_('View Dashboard'), content_type=dashboard_ct)
    business_types_ct = ContentType.objects.get_for_model(Category)
    Permission.objects.using(db).create(codename='ik_manage_category', name=_('Manage Category'), content_type=business_types_ct)
    profile_ct = ContentType.objects.get_for_model(Profile)
    Permission.objects.using(db).create(codename='ik_manage_payment', name=_('Manage Payment'), content_type=profile_ct)
    tax_ct = ContentType.objects.get_for_model(Tax)
    Permission.objects.using(db).create(codename='ik_manage_tax', name=_('Manage Tax'), content_type=tax_ct)
    project_ct = ContentType.objects.get_for_model(Project)
    Permission.objects.using(db).create(codename='ik_manage_project', name=_('Manage Project'), content_type=project_ct)
    payment_order_ct = ContentType.objects.get_for_model(PaymentOrder)
    Permission.objects.using(db).create(codename='ik_manage_payment_order', name=_('Manage Payment Order'), content_type=payment_order_ct)