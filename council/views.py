import logging
import json
import random
import string
from datetime import timedelta, datetime
from ikwen.accesscontrol.backends import UMBRELLA
from threading import Thread

# Create your views here.

import requests
from django.conf import settings
from django.contrib import messages
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView
from ikwen.core.models import Service

from ikwen.accesscontrol.models import Member
from ikwen.conf.settings import WALLETS_DB_ALIAS
from ikwen.core.constants import CONFIRMED, PENDING_FOR_PAYMENT
from ikwen.core.templatetags.url_utils import strip_base_alias
from ikwen.core.utils import increment_history_field
from ikwen.billing.decorators import momo_gateway_request, momo_gateway_callback
from ikwen.billing.models import MoMoTransaction, MTN_MOMO, Subscription, InvoiceItem, InvoiceEntry, Invoice, \
    IkwenInvoiceItem, NEW_INVOICE_EVENT
from ikwen.billing.utils import get_next_invoice_number

from ikwen.core.views import HybridListView, DashboardBase, ChangeObjectBase
from ikwen.core.utils import set_counters, get_service_instance, \
    get_mail_content, add_event
from ikwen.billing.invoicing.views import Payment

from council.admin import PaymentOrderAdmin, ProfileAdmin, TaxAdmin, PaymentAdmin, BannerAdmin, ProjectAdmin, \
    CategoryAdmin, CheckAdmin, WeddingApplicationAdmin, BuildingLicenceApplicationAdmin
from council.forms import ProfileForm
from council.models import PaymentOrder, Profile, Tax, Payment, Banner, Project, Category, Check, WeddingApplication, \
    BuildingLicenceApplication

logger = logging.getLogger('ikwen')


class Dashboard(DashboardBase):
    # template_name = 'council/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        return context


class Home(HybridListView):
    model = Tax
    model_admin = TaxAdmin
    template_name = 'council/show_tax_list.html'
    html_results_template_name = 'council/snippets/tax_list_results.html'

    def get_context_data(self, **kwargs):
        context = super(Home, self).get_context_data(**kwargs)
        config = get_service_instance().config
        created_profile = False
        if self.request.user.is_authenticated():
            if Profile.objects.filter(member=self.request.user):
                created_profile = True
        context['created_profile'] = created_profile
        context['banner'] = Banner.objects.first()
        context['currency_symbol'] = config.currency_symbol
        return context


class ChangeTax(ChangeObjectBase):
    model = Tax
    model_admin = TaxAdmin


class TaxList(HybridListView):
    model = Tax
    model_admin = TaxAdmin
    list_filter = ("type", )


class CheckList(HybridListView):
    model = Check
    list_filter = ("agent", "status",)


class ChangeCheck(ChangeObjectBase):
    model = Check
    model_admin = CheckAdmin


class ChangeBanner(ChangeObjectBase):
    model = Banner
    model_admin = BannerAdmin


class BannerList(HybridListView):
    model = Banner
    model_admin = BannerAdmin


class PaymentOrderList(HybridListView):
    model = PaymentOrder
    model_admin = PaymentOrderAdmin


class Receipt(TemplateView):
    template_name = 'council/receipt.html'

    def get_context_data(self, **kwargs):
        context = super(Receipt, self).get_context_data(**kwargs)
        config = get_service_instance().config
        receipt_id = self.kwargs.get('receipt_id')
        try:
            payment = Payment.objects.select_related('tax', 'member').get(pk=receipt_id)
        except Payment.DoesNotExist:
            raise Http404("Payment not found")
        context['currency_symbol'] = config.currency_symbol
        context['payment'] = payment
        context['member'] = payment.member
        context['amount'] = payment.amount
        context['subscription'] = Subscription.objects.get(member=payment.member, details=payment.id)
        return context


class PaymentList(HybridListView):
    model = Payment
    model_admin = PaymentAdmin


class ChangePaymentOrder(ChangeObjectBase):
    model = PaymentOrder
    model_admin = PaymentOrderAdmin

    def after_save(self, request, obj, *args, **kwargs):
        member_id = request.POST['member_id']
        member = Member.objects.get(pk=member_id)
        obj.provider = member
        obj.save()


class EditProfile(ChangeObjectBase):
    template_name = 'council/edit_profile.html'
    model = Profile
    model_admin = ProfileAdmin

    def get_context_data(self, **kwargs):
        context = super(EditProfile, self).get_context_data(**kwargs)
        context['category_list'] = Category.objects.all()
        try:
            context['profile'] = Profile.objects.get(member=self.request.user)
        except:
            pass
        return context

    # def get_object(self, **kwargs):
    #     if self.request.user.is_authenticated():
    #         try:
    #             profile = Profile.objects.get(member=self.request.user)
    #         except:
    #             profile = Profile(member=self.request.user)
    #         return profile

    def post(self, request, *args, **kwargs):
        context = super(EditProfile, self).get_context_data(**kwargs)
        profile_form = ProfileForm(request.POST)

        if profile_form.is_valid():
            id_number = profile_form.cleaned_data.get("id_number")
            location_lat = profile_form.cleaned_data.get("location_lat")
            location_lng = profile_form.cleaned_data.get("location_lng")
            formatted_address = profile_form.cleaned_data.get("formatted_address")
            taxpayer = profile_form.cleaned_data.get("taxpayer")
            company_name = profile_form.cleaned_data.get("company_name")
            member = profile_form.cleaned_data.get('member')
            business_type = profile_form.cleaned_data.get('business_type')
            # profile = self.get_object(**kwargs)
            if member:
                # try:
                member = Member.objects.get(pk=member)
                profile = Profile.objects.get(member=member)
                # except:
                #     profile = Profile.objects.create(member=member)
            if id_number:
                profile.id_number = id_number
            if location_lng:
                profile.location_lng = location_lng
            if location_lat:
                profile.location_lat = location_lat
            if formatted_address:
                profile.formatted_address = formatted_address
            if taxpayer:
                profile.taxpayer = taxpayer
            if company_name:
                profile.company_name = company_name
            if business_type:
                profile.business_type = business_type
            profile.save()
            return HttpResponseRedirect(reverse('home') + "?profile_created=yes")
            # else:
            #     return HttpResponseRedirect(reverse('ikwen:sign_in') + '?next=' + reverse('council:edit_profile'))
        else:
            context['form'] = profile_form
            return render(request, self.template_name, context)


class ChangeCategory(ChangeObjectBase):
    model = Category
    model_admin = CategoryAdmin


class CategoryList(HybridListView):
    model = Category


class ProjectList(HybridListView):
    model = Project
    model_admin = ProjectAdmin


class ChangeProject(ChangeObjectBase):
    model = Project
    model_admin = ProjectAdmin


class ChangeProfile(ChangeObjectBase):
    model = Profile
    model_admin = ProfileAdmin
    template_name = 'council/change_profile.html'


class ProfileList(HybridListView):
    model = Profile
    list_filter = ('business_type', )


class BookParking(TemplateView):
    template_name = 'council/book_parking.html'

    def get(self, request, *args, **kwargs):
        ref_id = self.request.GET.get('licence_plate')
        if ref_id:
            subscription = Subscription.objects.filter(reference_id=ref_id).last()
            try:
                check = Check.objects.get(agent=self.request.user, licence_plate=ref_id)
            except:
                check = Check.objects.create(agent=self.request.user, licence_plate=ref_id)
            if subscription:
                status = subscription.status
                check.customer = subscription.member
                check.save()
                if subscription.status != Service.ACTIVE:
                    check.status = status
                    check.save()
                return HttpResponse(json.dumps({'success': True, "status": status,
                                                'expired_date': subscription.expiry.strftime('%Y-%m-%d')}), content_type='application/json')
            else:
                check.status = Subscription.PENDING
                check.save()
                return HttpResponse(json.dumps({'error': True}), content_type='application/json')
        return super(BookParking, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BookParking, self).get_context_data(**kwargs)
        context['tax_list'] = [tax for tax in Tax.objects.filter(type='Parking')]
        context['check_count'] = Check.objects.filter(agent=self.request.user).count()
        return context


@momo_gateway_request
def set_momo_payment(request, *args, **kwargs):
    now = datetime.now()
    product_id = request.POST['product_id']
    product = Tax.objects.get(pk=product_id)
    # customer = request.user
    amount = product.cost
    # if product.type == 'Parking':
    ref_id = request.POST['licence_plate']
    customer_id = request.POST['customer_id']
    customer = get_object_or_404(Member, pk=customer_id)
    duration = [int(s) for s in product.duration_text.split() if s.isdigit()][0]
    # request.session['ref_id'] = ref_id
    if product.duration == 30:
        product.duration = 30 * duration
        product.save()
    billing_cycle = Service.MONTHLY
    if duration == 12:
        billing_cycle = Service.YEARLY
    if duration == 6:
        billing_cycle = Service.BI_ANNUALLY
    if duration == 3:
        billing_cycle = Service.QUARTERLY
    expiry = now + timedelta(days=product.duration)
    # amount = amount * duration
    payment = Payment.objects.create(member=customer, tax=product, method=Payment.MOBILE_MONEY,
                                     amount=amount, cashier=request.user)
    Subscription.objects.create(member=customer, reference_id=ref_id, product=product, since=now,
                                monthly_cost=float(product.cost/duration), billing_cycle=billing_cycle,
                                status=PENDING_FOR_PAYMENT, expiry=expiry, details=payment.id)
    notification_url = reverse('council:confirm_payment', args=(payment.id,))
    cancel_url = reverse('home')
    return_url = reverse('council:receipt', args=(payment.id, ))
    return payment, amount, notification_url, return_url, cancel_url


@momo_gateway_callback
def confirm_payment(request, *args, **kwargs):
    tx = kwargs['tx']
    payment = get_object_or_404(Payment, pk=tx.object_id)
    product = payment.tax
    # if product.type == 'Parking':
    subscription = Subscription.objects.get(member=payment.member, product=product, details=payment.pk)
    subscription.status = Service.ACTIVE
    subscription.save()

    item = InvoiceItem(label=_("Subscription of %s " % product.name), amount=tx.amount)
    short_description = subscription.created_on.strftime("%m/%d/%Y") + ' - ' + subscription.expiry.strftime("%m/%d/%Y")
    entry = InvoiceEntry(item=item, short_description=short_description, total=tx.amount,
                         quantity_unit='days',
                         quantity=product.duration)
    payment.entries = [entry]
    payment.payment_number = get_next_invoice_number()
    payment.status = CONFIRMED
    payment.processor_tx_id = tx.processor_tx_id
    payment.save()
    weblet = get_service_instance(check_cache=False)
    payer = payment.member
    # profile_payer = Profile.objects.get(member=payer)
    tx.username = subscription.member.username
    try:
        tx.save(using=WALLETS_DB_ALIAS)
    except:
        logger.error("Failed to change transaction username")
    check_list = Check.objects.filter(agent=request.user, licence_plate=subscription.reference_id).order_by("-id")
    if check_list:
        check = check_list[0]
        check.customer = subscription.member
        check.save()
    else:
        logger.info("Failed to get check list of with licence plate %s recorded by %s",
                    (subscription.licence_plate, request.user))
    email = product.email
    config = weblet.config
    if not email:
        email = config.contact_email
    if not email:
        email = weblet.member.email
    if email or payer.email:
        subject = _("Successful payment of %s" % product.name)
        try:
            html_content = get_mail_content(subject, template_name='council/mails/payment_notice.html',
                                            extra_context={'currency_symbol': config.currency_symbol,
                                                           'product': product,
                                                           'payer': payer,
                                                           # 'profile_payer': profile_payer,
                                                           'tx_date': tx.updated_on.strftime('%Y-%m-%d'),
                                                           'tx_time': tx.updated_on.strftime('%H:%M:%S')})
            sender = '%s <no-reply@%s>' % (weblet.project_name, weblet.domain)
            msg = EmailMessage(subject, html_content, sender, [payer.email])
            msg.bcc = [email]
            msg.content_subtype = "html"
            if getattr(settings, 'UNIT_TESTING', False):
                msg.send()
            else:
                Thread(target=lambda m: m.send(), args=(msg,)).start()
        except:
            logger.error("%s - Failed to send notice mail to %s." % (weblet, email), exc_info=True)
    return HttpResponse("Notification successfully received")


class Maps(TemplateView):
    template_name = 'council/maps.html'

    def get_context_data(self, **kwargs):
        context = super(Maps, self).get_context_data(**kwargs)
        context['settings'] = settings
        return context

    def get(self, request, *args, **kwargs):
        lat = request.GET.get('lat')
        lng = request.GET.get('lng')
        formatted_address = request.GET.get('formatted_address')
        if lat and lng and formatted_address:
            return HttpResponseRedirect(reverse('council:edit_profile') + "?lat=" + lat + "&lng=" + lng +
                                        "&formatted_address=" + formatted_address)
        return super(Maps, self).get(request, *args, **kwargs)


def notify_outdated_payment_orders(request, *args, **kwargs):
    now = datetime.now()
    weblet = get_service_instance()
    config = weblet.config
    for obj in PaymentOrder.objects.filter(due_date__lt=now.date()):
        if obj.last_notice_date:
            diff = now - obj.last_notice_date
            if diff.total_seconds() < 86400:
                continue
        # Send notification here by email
        subject = _("Outdated Payment Order")
        html_content = get_mail_content(subject, template_name='council/mails/outdate_payment_orders_notice.html',
                                        extra_context={'currency_symbol': config.currency_symbol,
                                                       'payment_order': obj,
                                                       'ref_id': obj.reference_id,
                                                       'provider': obj.provider,
                                                       'project_name': weblet.project_name,
                                                       'due_date': obj.due_date.strftime('%Y-%m-%d'),
                                                       'issue_date': obj.created_on.strftime('%Y-%m-%d')})
        sender = '%s <no-reply@%s>' % (weblet.project_name, weblet.domain)
        msg = EmailMessage(subject, html_content, sender, [config.contact_email])
        msg.cc = ["silatchomsiaka@gmail.com"]
        msg.content_subtype = "html"
        if getattr(settings, 'UNIT_TESTING', False):
            msg.send()
        else:
            Thread(target=lambda m: m.send(), args=(msg,)).start()
    messages.success(request, _('Email sent'),)

    return HttpResponseRedirect(reverse('home'))


class SubmitWeddingApplication(ChangeObjectBase):
    model = WeddingApplication
    model_admin = WeddingApplicationAdmin
    template_name = 'council/wedding_application.html'

    def after_save(self, request, obj, *args, **kwargs):
        number = get_next_invoice_number()
        now = datetime.now()
        invoice_total = 0
        invoice_entries = []
        service = get_service_instance()
        item_fees = IkwenInvoiceItem(label=_("Wedding fees"), amount=72000)
        entry_fees = InvoiceEntry(item=item_fees, total=72000)
        item_cards = IkwenInvoiceItem(label=_("Cards"), amount=2000)
        entry_cards = InvoiceEntry(item=item_cards, total=2000)
        item_cardboard_folder = IkwenInvoiceItem(label=_("Cardboard folder"), amount=100)
        entry_cardboard_folder = InvoiceEntry(item=item_cardboard_folder, total=100)
        invoice_entries.extend([entry_fees, entry_cards, entry_cardboard_folder])
        member = self.request.user
        for entry in invoice_entries:
            invoice_total += entry.item.amount * entry.quantity
        invoice = Invoice(member=member, subscription=service, amount=invoice_total, number=number, last_reminder=now,
                          reminders_sent=1, is_one_off=True, entries=invoice_entries, date_issued=now,
                          due_date=now + timedelta(days=14))
        invoice.save(using=UMBRELLA)
        invoice.save()
        vendor = get_service_instance()
        add_event(vendor, NEW_INVOICE_EVENT, member=member, object_id=invoice.id)
        sender = '%s <no-reply@ikwen.com>' % service.project_name
        invoice_url = reverse('billing:invoice_detail', args=(invoice.id,))
        invoice_url = service.url + invoice_url
        subject = _("Congratulations!!! Your wedding application was successfully created")
        message = _("You just submit wedding application, pay the invoice for it to take into account")
        staff_list = list(set([member.email for member in Member.objects.filter(is_staff=True, is_superuser=True)]))
        try:
            html_content = get_mail_content(subject, message, template_name="council/mails/wedding_application.html",
                                            extra_context={'service_activated': service, 'config': service.config,
                                                           'invoice': invoice, 'member': member,
                                                           'invoice_url': invoice_url})
            msg = EmailMessage(subject, html_content, sender, [member.email])
            msg.bcc = staff_list
            msg.content_subtype = "html"
            Thread(target=lambda m: m.send(), args=(msg,)).start()
        except:
            logger.error("Failed to send email to %s" % member.full_name, exc_info=False)
        return HttpResponseRedirect(reverse("council:buy_wedding_application") + "?invoice_id=" + invoice.id)


class BuyWeddingApplication(TemplateView):
    template_name = 'council/buy_wedding_application.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        invoice_id = request.GET.get("invoice_id")
        invoice_url = reverse('billing:invoice_detail', args=(invoice_id,))
        context["invoice_url"] = invoice_url
        # invoice_url = 'http://www.ikwen.com' + invoice_url
        return self.render_to_response(context)


class SubmitBuildingLicenceApplication(ChangeObjectBase):
    model = BuildingLicenceApplication
    model_admin = BuildingLicenceApplicationAdmin
    template_name = 'council/submit_building_licence.html'

    def after_save(self, request, obj, *args, **kwargs):
        number = get_next_invoice_number()
        now = datetime.now()
        invoice_total = 0
        invoice_entries = []
        service = get_service_instance()
        tax_stamp_item = IkwenInvoiceItem(label=_("The tax stamp"), amount=1000)
        entry_tax = InvoiceEntry(item=tax_stamp_item, total=1000)
        item_estimate = IkwenInvoiceItem(label=_("1% of the estimated cost"), amount=obj.estimated_cost*1/100)
        entry_estimate = InvoiceEntry(item=item_estimate, total=obj.estimated_cost*1/100)
        item_entry_administrative = IkwenInvoiceItem(label=_("Administrative procedure"),
                                                     amount=obj.administrative_procedure_cost)
        entry_administrative_procedure = InvoiceEntry(item=item_entry_administrative,
                                                      total=obj.administrative_procedure_cost)
        invoice_entries.extend([entry_tax, entry_estimate, entry_administrative_procedure])
        for entry in invoice_entries:
            invoice_total += entry.item.amount * entry.quantity
        invoice = Invoice(member=request.user, subscription=service, amount=invoice_total, number=number,
                          last_reminder=now, reminders_sent=1, is_one_off=True, entries=invoice_entries, date_issued=now,
                          due_date=now + timedelta(days=14))
        invoice.save(using=UMBRELLA)
        invoice.save()
        vendor = get_service_instance()
        member = self.request.user
        add_event(vendor, NEW_INVOICE_EVENT, member=member, object_id=invoice.id)
        sender = '%s <no-reply@ikwen.com>' % service.project_name
        invoice_url = reverse('billing:invoice_detail', args=(invoice.id,))
        invoice_url = vendor.url + invoice_url
        subject = _("Congratulations!!! You successfully applied for a builder licence")
        message = _("You just submit builder licence, pay the invoice for it to take into account")
        staff_list = list(set([member.email for member in Member.objects.filter(is_staff=True, is_superuser=True)]))
        try:
            html_content = get_mail_content(subject, message, template_name="council/mails/building_licence.html",
                                            extra_context={'service_activated': service, 'config': service.config,
                                                           'invoice': invoice, 'member': member,
                                                           'invoice_url': invoice_url})
            msg = EmailMessage(subject, html_content, sender, [member.email])
            msg.bcc = staff_list
            msg.content_subtype = "html"
            Thread(target=lambda m: m.send(), args=(msg,)).start()
        except:
            logger.error("Failed to send email to %s" % member.full_name, exc_info=False)
        return HttpResponseRedirect(reverse("council:buy_building_licence") + "?invoice_id=" + invoice.id)


class BuyBuildingLicence(TemplateView):
    template_name = 'council/buy_building_licence.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        invoice_id = request.GET.get("invoice_id")
        invoice_url = reverse('billing:invoice_detail', args=(invoice_id,))
        context["invoice_url"] = invoice_url
        # invoice_url = 'http://www.ikwen.com' + invoice_url
        return self.render_to_response(context)


class WeddingApplicationList(HybridListView):
    model = WeddingApplication


class ChangeWeddingApplication(ChangeObjectBase):
    model = WeddingApplication
    model_admin = WeddingApplicationAdmin


class BuildingLicenceApplicationList(HybridListView):
    model = BuildingLicenceApplication


class ChangeBuildingLicenceApplication(ChangeObjectBase):
    model = BuildingLicenceApplication
    model_admin = BuildingLicenceApplicationAdmin

