
from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required, permission_required

from council.views import Home, PaymentOrderList, Dashboard, ChangeTax, TaxList, Receipt, ChangePaymentOrder, \
    EditProfile, ProfileList, set_momo_payment, confirm_payment, PaymentList, BookParking, Maps,\
    notify_outdated_payment_orders, ChangeProfile, ChangeBanner, BannerList, ProjectList, ChangeProject, CategoryList,\
    ChangeCategory, ChangeCheck, CheckList, SubmitBuildingLicenceApplication, SubmitWeddingApplication, \
    BuyWeddingApplication, BuyBuildingLicence, WeddingApplicationList, ChangeWeddingApplication, \
    BuildingLicenceApplicationList, ChangeBuildingLicenceApplication

urlpatterns = patterns(
    '',

    url(r'^kmth/dashboard/$', permission_required('council.ik_view_dashboard')(Dashboard.as_view()), name='dashboard'),

    url(r'^kmth/payments/$', permission_required('council.ik_manage_payment')(PaymentList.as_view()), name='payment_list'),

    url(r'^kmth/categoryList/$', permission_required('council.ik_manage_category')(CategoryList.as_view()), name='category_list'),
    url(r'^kmth/changeCategory/$', permission_required('council.ik_manage_category')(ChangeCategory.as_view()), name='change_category'),
    url(r'^kmth/changeCategory/(?P<object_id>[-\w]+)$', permission_required('council.ik_manage_category')(ChangeCategory.as_view()), name='change_category'),

    url(r'^kmth/taxList/$', permission_required('council.ik_manage_tax')(TaxList.as_view()), name='tax_list'),
    url(r'^kmth/changeTax/$', permission_required('council.ik_manage_tax')(ChangeTax.as_view()), name='change_tax'),
    url(r'^kmth/changeTax/(?P<object_id>[-\w]+)$', permission_required('council.ik_manage_tax')(ChangeTax.as_view()), name='change_tax'),

    url(r'^kmth/changeProfile/$', permission_required('council.ik_view_profile')(ChangeProfile.as_view()), name='change_profile'),
    url(r'^kmth/changeProfile/(?P<object_id>[-\w]+)/$', permission_required('council.ik_view_profile')(ChangeProfile.as_view()), name='change_profile'),
    url(r'^kmth/profileList/$', permission_required('council.ik_view_profile')(ProfileList.as_view()), name='profile_list'),

    url(r'^kmth/weddingApplicationList/$', permission_required('council.ik_manage_wedding')(WeddingApplicationList.as_view()),
        name='wedding_application_list'),
    url(r'^kmth/changeWeddingApplication/$', permission_required('council.ik_manage_wedding')(ChangeWeddingApplication.as_view()),
        name='change_weddingapplication'),
    url(r'^kmth/changeWeddingApplication/(?P<object_id>[-\w]+)$',
        permission_required('council.ik_manage_wedding')(ChangeWeddingApplication.as_view()),
        name='change_weddingapplication'),

    url(r'^kmth/buildingLicenceList/$', permission_required('council.ik_manage_building_licence')(BuildingLicenceApplicationList.as_view()),
        name='building_licence_list'),
    url(r'^kmth/changeBuildingLicence/$',
        permission_required('council.ik_manage_building_licence')(ChangeBuildingLicenceApplication.as_view()),
        name='change_buildinglicenceapplication'),
    url(r'^kmth/changeBuildingLicence/(?P<object_id>[-\w]+)$',
        permission_required('council.ik_manage_building_licence')(ChangeBuildingLicenceApplication.as_view()),
        name='change_buildinglicenceapplication'),

    url(r'^kmth/changeProject/$', permission_required('council.ik_manage_project')(ChangeProject.as_view()), name='change_project'),
    url(r'^kmth/changeProject/(?P<object_id>[-\w]+)/$', permission_required('council.ik_manage_project')(ChangeProject.as_view()), name='change_project'),
    url(r'^kmth/projectList/$', permission_required('council.ik_manage_project')(ProjectList.as_view()), name='project_list'),

    url(r'^kmth/paymentOrderList$', permission_required('council.ik_manage_payment_order')(PaymentOrderList.as_view()), name='paymentorder_list'),
    url(r'^kmth/changePaymentOrder/$', permission_required('council.ik_manage_payment_order')(ChangePaymentOrder.as_view()), name='change_paymentorder'),
    url(r'^kmth/changePaymentOrder/(?P<object_id>[-\w]+)$', permission_required('council.ik_manage_payment_order')(ChangePaymentOrder.as_view()), name='change_paymentorder'),

    url(r'^kmth/bannerList/$', permission_required('council.ik_manage_tax')(BannerList.as_view()), name='banner_list'),
    url(r'^kmth/changeBanner/$', permission_required('council.ik_manage_tax')(ChangeBanner.as_view()), name='change_banner'),
    url(r'^kmth/changeBanner/(?P<object_id>[-\w]+)$', permission_required('council.ik_manage_tax')(ChangeBanner.as_view()), name='change_banner'),

    url(r'^kmth/checkList/$', permission_required('council.ik_manage_tax')(CheckList.as_view()), name='check_list'),
    url(r'^kmth/changeCheck/$', permission_required('council.ik_manage_tax')(ChangeCheck.as_view()), name='change_check'),
    url(r'^kmth/changeCheck/(?P<object_id>[-\w]+)$', permission_required('council.ik_manage_tax')(ChangeCheck.as_view()), name='change_check'),


    # url(r'^home/$', Home.as_view(), name='home'),
    url(r'^profile/edit$', login_required(EditProfile.as_view()), name='edit_profile'),
    url(r'^maps$', login_required(Maps.as_view()), name='maps'),
    url(r'^wedding/apply$', login_required(SubmitWeddingApplication.as_view()), name='submit_wedding_application'),
    url(r'^wedding/application/buy$', login_required(BuyWeddingApplication.as_view()), name='buy_wedding_application'),
    url(r'^buildingLicence/apply$', login_required(SubmitBuildingLicenceApplication.as_view()),
        name='building_licence_application'),
    url(r'^buildingLicence/application/buy$', login_required(BuyBuildingLicence.as_view()),
        name='buy_building_licence'),
    url(r'^receipt/(?P<receipt_id>[-\w]+)/$', login_required(Receipt.as_view()), name='receipt'),
    url(r'^bookParking/$', login_required(BookParking.as_view()), name='book_parking'),

    url(r'^set_momo_payment$', set_momo_payment),
    url(r'^confirm_payment/(?P<object_id>[-\w]+)$', confirm_payment, name='confirm_payment'),
    url(r'^confirm_payment/(?P<object_id>[-\w]+)/(?P<signature>[-\w]+)$', confirm_payment, name='confirm_payment'),

    url(r'^notif$', notify_outdated_payment_orders, name='notify_outdated_payment_orders'),
)
