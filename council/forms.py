from django import forms

from ikwen.accesscontrol.models import Member

from council.models import Category, ActivityCategory


class ProfileForm(forms.Form):
    member = forms.ModelChoiceField(queryset=Member.objects.all())
    id_number = forms.CharField(max_length=100, required=True,
                                error_messages={'required': 'Please enter your ID card number'})
    location_lat = forms.FloatField(required=False)
    location_lng = forms.FloatField(required=False)
    formatted_address = forms.CharField(max_length=250, required=False)
    taxpayer = forms.CharField(max_length=100, required=False)
    company_name = forms.CharField(max_length=100, required=False)
    business_type = forms.ModelChoiceField(queryset=Category.objects.all())
    # activity_category = forms.ModelChoiceField(queryset=ActivityCategory)


