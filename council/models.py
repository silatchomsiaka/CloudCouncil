from django.conf.global_settings import LANGUAGES
from django.contrib.humanize.templatetags.humanize import intcomma
from django.db import models

# Create your models here.
from django.db.models import Model
from django.utils.translation import gettext_lazy as _
from djangotoolbox.fields import ListField, EmbeddedModelField

from ikwen.core.utils import slice_watch_objects, rank_watch_objects, add_database, set_counters, get_service_instance, \
    get_model_admin_instance, clear_counters, get_mail_content, XEmailMessage, add_event

from ikwen.core.models import Model, AbstractConfig, Service
from ikwen.core.fields import MultiImageField, FileField
# from ikwen.core.constants import PAYMENT_STATUS_CHOICES
from ikwen.accesscontrol.models import Member

from ikwen.billing.models import AbstractProduct, AbstractPayment
from ikwen_kakocase.kakocase.models import ProductCategory


PENDING = "Pending"
OVERDUE = "Overdue"
EXCEEDED = "Exceeded"
PAID = "Paid"
INVOICE_STATUS_CHOICES = (
    (PENDING, _("Pending")),
    (OVERDUE, _("Overdue")),
    (EXCEEDED, _("Exceeded")),
    (PAID, _("Paid")),
)


class Category(Model):
    name = models.CharField(max_length=100, null=True, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = _('Categories')


class ActivityCategory(Model):
    name = models.CharField(max_length=100, null=True, blank=True)

    def __unicode__(self):
        return self.name


class Tax(AbstractProduct):
    TAX_CHOICES = (
        ('Commercial', _('Commercial')),
        ('Parking', _('Parking'))
    )
    phone = models.CharField(max_length=60, blank=True, null=True,
                             help_text="Contact phone of support team")
    email = models.EmailField(blank=True, null=True,
                              help_text="Contact email of support team")
    provider_share_rate = models.FloatField(default=10)
    callback = models.CharField(max_length=255, blank=True,
                                help_text="Callback that is run after completion of payment of this product.")
    template_name = models.CharField(max_length=150, blank=True,
                                     help_text="Template of the detail page of the Product.")
    lang = models.CharField(max_length=30, choices=LANGUAGES, default='en')
    keywords = models.CharField(max_length=255, db_index=True, blank=True, null=True,
                            help_text="Search tags")
    type = models.CharField(max_length=150, choices=TAX_CHOICES, default='Commercial')

    class Meta:
        verbose_name_plural = _('Taxes')


class PaymentOrder(Model):
    provider = models.ForeignKey(Member, blank=True, null=True)
    reference_id = models.CharField(_('Reference ID'), max_length=255, null=True, blank=True)
    amount = models.IntegerField(_('Amount'), default=0)
    due_date = models.DateField(_('Due date'))
    last_notice_date = models.DateTimeField(_('Last notice date'), null=True, blank=True)
    status = models.CharField(_('Status'), max_length=100, choices=INVOICE_STATUS_CHOICES)

    def __unicode__(self):
        return self.provider.full_name

    def get_obj_details(self):
        return 'Ref: %s : <strong style="color: #222">%s</strong>' % (self.reference_id, intcomma(self.amount))


class Payment(AbstractPayment):
    member = models.ForeignKey(Member)
    # model = models.CharField(max_length=24, db_index=True)
    # object_id = models.CharField(max_length=24, db_index=True)
    tax = models.ForeignKey(Tax)
    status = models.CharField(max_length=100, db_index=True, default=PENDING)
    entries = ListField(EmbeddedModelField('billing.InvoiceEntry'), null=True, blank=True)
    payment_number = models.CharField(max_length=10, null=True, blank=True, default='')

    # def __unicode__(self):
    #     if self.model:
    #         return self.model.split('.')[1]
    #     return "N/A"

    def __unicode__(self):
        return "%s of  XAF %s:  %s " % (self.tax.name, intcomma(self.tax.cost), self.member.full_name)


class Profile(Model):
    member = models.ForeignKey(Member)
    id_number = models.CharField(_('ID Card Number'), max_length=100, db_index=True)
    location_lat = models.FloatField(default=0.0, null=True, blank=True, db_index=True)
    location_lng = models.FloatField(default=0.0, null=True, blank=True, db_index=True)
    formatted_address = models.CharField(_('Address'), max_length=250, default='', null=True, blank=True)
    taxpayer = models.CharField(_('Taxpayer Number'),  max_length=100, blank=True, null=True, db_index=True)
    business_type = models.ForeignKey(Category, verbose_name=_('Business Type'), null=True, blank=True)
    # activity_category = models.ForeignKey(ActivityCategory, verbose_name=_('Activity category'), blank=True, null=True)
    company_name = models.CharField(_('Company Name'), max_length=100, blank=True, null=True, db_index=True)

    def __unicode__(self):
        return self.member.full_name

    def get_obj_details(self):
        return self.business_type


class Banner(Model):
    UPLOAD_TO = 'Banner'
    title_header = models.CharField(_("Title's header"), max_length=50, blank=True, null=True)
    image = MultiImageField(_('Banner image'), allowed_extensions=['jpeg', 'png', 'jpg'],
                            upload_to=UPLOAD_TO, required_width=1920, null=True)

    def __unicode__(self):
        return self.title_header


class Project(Model):
    name = models.CharField(_("Project's name"), max_length=150)
    leader = models.ForeignKey(Member)
    cost = models.IntegerField(_("Cost"), default=0)
    description = models.TextField(_("Project description"))
    attachment = FileField(_('Attachment'), allowed_extensions=['doc', 'docx', 'pdf', 'ppt', 'odt'], upload_to='Projects')


class Check(Model):
    licence_plate = models.CharField(_("Licence plate"), max_length=150)
    customer = models.ForeignKey(Member, blank=True, null=True)
    agent = models.ForeignKey(Member)
    status = models.CharField(_("Subscription Status"), max_length=150)

    def __unicode__(self):
        return "%s" % self.agent

    def get_obj_details(self):
        return "%s - %s" % (self.licence_plate, self.customer)


class WeddingApplication(Model):
    WEDDING_APPLICATIONS = "wedding/applications"
    husband = FileField(_("Husband's ID"), allowed_extensions=['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg', 'odt'],
                        upload_to='IDs')
    spouse = FileField(_("Spouse's ID"),
                       allowed_extensions=['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg', 'odt'],
                       upload_to='IDs')
    family_head1 = FileField(_("ID of the husband family's head"),
                             allowed_extensions=['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg', 'odt'],
                             upload_to='IDs')
    family_head2 = FileField(_("ID of the spouse family's head"),
                             allowed_extensions=['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg', 'odt'],
                             upload_to='IDs')
    image = MultiImageField(help_text=_("Half photo card in 192x192 format"), upload_to="Photos", blank=True,
                            null=True, max_size=600, small_size=200, thumb_size=100, required_height=192,
                            required_width=192)
    applicant = models.ForeignKey(Member)

    def __unicode__(self):
        return self.applicant.full_name


class BuildingLicenceApplication(Model):
    BUILDING_LICENCE_APPLICATIONS = "building_licence/applications"
    property_certificate = FileField(_("Property certificate"),
                                     allowed_extensions=['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg', 'odt'],
                                     upload_to='Certificates')
    urbanism_certificate = FileField(_("Urbanism certificate"),
                                     allowed_extensions=['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg', 'odt'],
                                     upload_to='Certificates')
    ground_plan = MultiImageField(upload_to=BUILDING_LICENCE_APPLICATIONS + "/Plans", blank=True, null=True,
                                  max_size=600, small_size=200, thumb_size=100)
    location_plan = MultiImageField(upload_to=BUILDING_LICENCE_APPLICATIONS + "/Plans", blank=True, null=True,
                                    max_size=600, small_size=200, thumb_size=100)
    estimate = FileField(_("Estimate"), allowed_extensions=['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg', 'odt'],
                         upload_to=BUILDING_LICENCE_APPLICATIONS + '/Estimates')
    estimated_cost = models.IntegerField(_("Estimated cost"), default=0)
    administrative_procedure_cost = models.IntegerField(_("Administrative procedure cost"), default=0)
    applicant = models.ForeignKey(Member)


    def __unicode__(self):
        return self.applicant.full_name

    def _get_image(self):
        return self.ground_plan
    image = property(_get_image)

    def get_obj_details(self):
        return _("Estimate: %s XAF") % intcomma(self.estimated_cost)